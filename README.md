# Classic LAMP environment built on Docker-compose

Currently this project consists of:

- CentOS 6
- Apache 2.2
- MySQL 5.6
- PHP 5.6
- Phpmyadmin
